const express = require("express");
const multer = require('multer');
const path = require('path');
const {MongoClient} = require('mongodb');

// App setup
const PORT = 7000;
const app = express();

// SET ACTIVE ENVIRONMENT
let environment = process.env.NODE_ENV || 'development';

// MongoDB connection URL
const MONGO_DB_URL = `mongodb+srv://root:3yJ4nCbSaBAn87Fn@testone.vr8tl.mongodb.net/databoxes?retryWrites=true&w=majority`;
const MONGO_DB_NAME = 'databoxes';

const server = app.listen(PORT, () => {
  console.log("server is running on port", PORT);
});

const fileFilter = function(request, file, callback) {
  // File format to accept
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF|svg|SVG)$/)) {
    request.fileValidationError = 'Only ----- files are allowed!';
      return callback(new Error('Only ------ files are allowed!'), false);
  }
  callback(null, true);
};

// Use connect method to connect to the server
const MgDBclient = new MongoClient(MONGO_DB_URL, { useUnifiedTopology: true });
try {
  MgDBclient.connect();
  console.log("MongoDB connected successfully to server");
} catch (e) {
  console.error(e);
} finally {
  MgDBclient.close();
}

// Set storage location for files
const storage = multer.diskStorage({
  destination: function(request, file, callback) {
      callback(null, `uploads_${environment}/`);
  },
  // By default, multer removes file extensions so let's add them back
  filename: function(request, file, callback) {
      callback(null, file.originalname);
  }
});

app.post('/upload_file', function (request, response) {
  // 'file_data' is the name of our file input field in the HTML form
  let upload = multer({ storage: storage, fileFilter: fileFilter }).single('file_data');
  upload(request, response, function(err) {
      if (request.fileValidationError) {
          return response.send(request.fileValidationError);
      } else if (!request.file) {
          return response.send('Please select an image to upload');
      } else if (err instanceof multer.MulterError) {
          return response.send(err);
      } else if (err) {
          return response.send(err);
      }
      // Save uploaded file details in MongoDB
      let file_data = {
        "name": request.file.originalname.split(".")[0],
        "size": request.file.size
      }
      MgDBclient.db(MONGO_DB_NAME).collection(`files_uploaded_${environment}`).insertOne(file_data);
      console.log(`File has been saved successfully.`);

      // Display uploaded file to client side as reponse
      response.send(`You have uploaded this file: <hr/><img src="${request.file.path}" width="500"><hr /><a href="./test.html">Upload another image</a>`);
  });
});

// Static files
app.use(express.static(__dirname));